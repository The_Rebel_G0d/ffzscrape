const
    logger = new (require("trg_logger"))("ffzscrape"),
    _ = require("lodash"),
    util = require("util"),
    request = require("requestretry"),
    rl = require("readline"),
    cli = require("minimist")(process.argv.slice(2)),
    fs = require("fs"),
    path = require("path"),
    chalk = require("chalk");
require("promise.prototype.finally").shim();

let outputLocation = "./ffz.json";
let max_pages = -1;
let force_output = false;

if (!util.isNullOrUndefined(cli["O"])) outputLocation = String(cli["O"]);
else if (!util.isNullOrUndefined(cli["output"])) outputLocation = String(cli["output"]);

if (!util.isNullOrUndefined(cli["y"]) || !util.isNullOrUndefined(cli["Y"]) || !util.isNullOrUndefined(cli["force"])) force_output = true;

if (util.isNumber(cli["M"])) max_pages = Math.floor(cli["M"]);
else if (util.isNumber(cli["max-pages"])) max_pages = Math.floor(cli["max-pages"]);
if (!util.isNumber(max_pages) || isNaN(max_pages) || max_pages <= 0) max_pages = -1;

let cpage = 0,
    emotes = [];

function status(msg) {
    rl.clearLine(process.stdout, 0);
    rl.cursorTo(process.stdout, 0);
    process.stdout.write(msg);
}



prereqs().then(() => {
    logger.info(`Scraping FFZ API. Will grab ${max_pages == -1 ? "all" : max_pages} page(s).`);
    doWork();
}).catch(() => {
    logger.warn("Some pre-checks didn't pass, exiting.");
    process.exit(1);
});


function doWork() {
    if (max_pages > -1) {
        if (cpage >= max_pages) {
            process.stdout.write(require("os").EOL);
            let outputPath = path.normalize(outputLocation);
            require("fs").writeFile(outputPath, JSON.stringify(emotes), function(err) {
                if (err) {
                    logger.error(err);
                    logger.error("Failed to dump file! Don't know what to do with the data now, just going to exit.");
                } else {
                    logger.info(`Flushed file to ${outputPath}`);
                }
            });
            return;
        }
    }
    handleFetch().then(doWork).catch(err => {
        logger.error(err);
        logger.error("Failed to handleFetch, reprocessing");
        reprocess();
    });
}
function reprocess() {
    --cpage;
    return doWork();
}

function handleFetch() {
    return new Promise((resolve, reject) => {
        let ourPage = ++cpage;
        let url = getURL(ourPage);
        request({
            url,
            maxAttempts: 3,
            retryDelay: 2500,
            retryStrategy: request.RetryStrategies.HTTPOrNetworkError,
            fullResponse: false
        }).then(resp => {
            try {
                let data = JSON.parse(resp);
                if (data && util.isArray(data.emoticons)) {
                    if (max_pages == -1) max_pages = data._pages;
                    status(`Processing page ${ourPage}/${max_pages} (${max_pages > 0 ? (Math.ceil(ourPage * 100 / max_pages)) : 0}%)`);
                    _.each(data.emoticons, emote => {
                        let parsedLinks = {small: null, medium: null, large: null};
                        if (util.isObject(emote.urls)) {
                            let keys = Object.keys(emote.urls);
                            if (keys.length > 0) {
                                parsedLinks.small = `https:${emote.urls[keys[0]]}`;
                                if (keys.length > 1) {
                                    for (let i = 1; i < keys.length; i++) {
                                        if (parsedLinks.medium == null) {
                                            parsedLinks.medium = `https:${emote.urls[keys[i]]}`;
                                        } else if (parsedLinks.large == null) {
                                            parsedLinks.large = `https:${emote.urls[keys[i]]}`;
                                            break;
                                        }
                                    }
                                }
                            }

                            emotes.push({
                                id: emote.id,
                                name: emote.name,
                                links: parsedLinks
                            });
                        } else {
                            logger.warn("No URLs for emote", emote.id, emote.name);
                        }
                    });
                    setTimeout(() => resolve(), 1000);
                } else {
                    logger.error("Page", ourPage, "gave invalid data (emoticons wasn't array). Resolving so we don't reprocess.", url);
                    setTimeout(() => resolve(), 1000);
                }
            } catch (e) {
                setTimeout(() => resolve(), 1000);
                logger.error(e);
                logger.error(resp);
                logger.error("Invalid response from server");
            }
        }).catch(reject);
    });
}

function prereqs() {
    return new Promise((resolve, reject) => {
        if (force_output) {
            resolve();
        } else {
            fs.access(outputLocation, err => {
                if (!err) {
                    promptAndGetResponse(chalk["white"]["bgYellow"]("The output file already exists. Would you like to overwrite? [Y/n]:")).then(chunk => {
                        //  10 => LF
                        //  13 => CR (windows 'enter' is CRLF. chunk[0] will be CR, chunk[1] will be LF.)
                        //  89 =>  Y
                        // 121 =>  y
                        if ([10, 13, 89, 121].includes(chunk.charCodeAt(0))) {
                            resolve();
                        } else {
                            reject();
                        }
                    }).catch(e => logger.error(e)).finally(() => {
                        reject(); //if we hit finally, and haven't resolved yet, we should reject to be safe.
                    });
                } else resolve();
            });
        }
    });
}

function promptAndGetResponse(prompt) {
    return new Promise((resolve, reject) => {
        process.stdin.once("data", chunk => {
            process.stdin.pause();
            resolve(new Buffer(chunk).toString());
        });
        process.stdout.write(prompt);
        process.stdin.resume();
    });
}

function getURL(page) {
    return `https://api.frankerfacez.com/v1/emoticons?page=${page}&per_page=200&private=on`;
}